const db = require('../db')

const getCollection = () => db.get().collection('posts')

module.exports.getAllAuthors = () =>
	getCollection()
		.aggregate([{$group: {_id: '$author'}}, {$sort: {_id: 1}}])
		.toArray()
		.then(authors => authors.map(author => ({name: author._id})))