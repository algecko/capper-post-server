const db = require('../db')

const getCollection = () => db.get().collection('posts')

module.exports.getPostsPerDate = () =>
	getCollection()
		.aggregate([{
			$addFields: {
				groupingDate: {
					$dateToString: {
						format: '%Y-%m-%d',
						date: '$date'
					}
				}
			}
		}, {$group: {_id: '$groupingDate', postCount: {$sum: 1}}}])
		.toArray()
		.then(groups => groups.map(group => {
			return ({day: group._id, postCount: group.postCount})
		}))