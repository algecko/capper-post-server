const {like, unlike} = require('./likes')
const {findIds, getPost} = require('./postsSimple')
const {findPosts} = require('./pagedPosts')
const scripts = require('./scripts')

module.exports = {
	like,
	unlike,
	findIds,
	getPost,
	findPosts,
	scripts
}