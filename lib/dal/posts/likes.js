const {getCollection} = require('./shared')

const like = (id) => changeLikes(id, 1)

const unlike = (id) => changeLikes(id, -1)

const changeLikes = (id, likeChange) =>
	getCollection()
		.findOneAndUpdate({id}, {$inc: {likes: likeChange}}, {returnOriginal: false})
		.then(res => res.value)

module.exports = {like, unlike}