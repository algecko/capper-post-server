const escapeStringRegexp = require('escape-string-regexp')


const tagsFilter = tags => ({$or: tags.map(tagFilter)})
const tagFilter = tag => ({tags: new RegExp(`^${escapeStringRegexp(tag)}$`, 'i')})

const authorsFilter = authorNames => ({$or: authorNames.map(authorFilter)})
const authorFilter = authorName => ({author: new RegExp(`^${escapeStringRegexp(authorName)}$`, 'i')})


module.exports = (options) => {
	const {after, before, authors = [], tags = [], dateRange, text, sort, minSize, maxSize} = options

	let filters = []
	const additionalFieldFilters = []

	if (sort === 'best') {
		if (after)
			additionalFieldFilters.push({likesWithDate: {$lt: after}})
		if (before)
			additionalFieldFilters.push({likesWithDate: {$gt: before}})
	} else if (sort !== 'random') {
		if (after)
			filters.push({date: {$lt: after}})
		if (before)
			filters.push({date: {$gt: before}})
	}

	if (authors.length > 0)
		filters = filters.concat(authorsFilter(authors))

	if (tags.length > 0)
		filters.push(tagsFilter(tags))

	if (dateRange) {
		filters.push({
			$and: [
				{date: {$gte: dateRange.rangeStart}},
				{date: {$lt: dateRange.rangeEnd}}
			]
		})
	}
	if (text && text.length > 0) {
		filters.push({$text: {$search: text}})
	}

	if (minSize)
		filters.push({size: {$gte: minSize}})
	if (maxSize)
		filters.push({size: {$lte: maxSize}})

	return {
		mainFilter: filters.length > 0 ? {$and: filters} : {},
		additionalFieldFilter: additionalFieldFilters.length === 0 ? {} : {$and: additionalFieldFilters}
	}
}