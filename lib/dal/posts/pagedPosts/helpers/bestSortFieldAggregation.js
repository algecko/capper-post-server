const CRITERION_NAME = 'likesWithDate'

const field = {
	[CRITERION_NAME]: {
		$add:
			[
				{$minute: '$date'},
				{$multiply: [{$hour: '$date'}, 10]},
				{$multiply: [{$dayOfYear: '$date'}, 10000]},
				{$multiply: [{$year: '$date'}, 10000000]},
				{$multiply: [{$ifNull: ['$likes', 0]}, 100000000000]}
			]
	}
}

module.exports = {CRITERION_NAME, field}