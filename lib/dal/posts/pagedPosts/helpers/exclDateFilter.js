module.exports = andedFilter => {
	if (!andedFilter.$and)
		return {}

	const fields = andedFilter.$and.filter(kvp => Object.keys(kvp)[0] !== 'date')
	if (fields.length === 0)
		return {}

	return {$and: fields}
}