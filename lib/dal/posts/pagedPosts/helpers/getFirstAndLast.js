const searchOptionsHelper = require('./postSearchOptionsHelper')
const {getCollection} = require('../../shared')
const bestSortFieldAggregator = require('./bestSortFieldAggregation')
const exclDateFilter = require('./exclDateFilter')

const NodeCache = require('node-cache')
const cache = new NodeCache({stdTTL: 300, checkperiod: 350})

const LAST_CACHE_KEY = 'last'
const FIRST_CACHE_KEY = 'first'

module.exports = async (options, filter) => {
	if (searchOptionsHelper.isRandom(options))
		return {
			first: null,
			last: null
		}

	const myFilter = exclDateFilter(filter)


	return {
		first: await getItem(true, options, myFilter),
		last: await getItem(false, options, myFilter)
	}
}

const getCacheSuffix = (options) => {
	const criteria = {...options}
	delete criteria.groupedItems
	delete criteria.before
	delete criteria.after

	return Object.keys(criteria).map(key => `${key}=${criteria[key]}`).join(',')
}

const getItem = async (first, options, filter) => {
	const cacheKey = `${first ? FIRST_CACHE_KEY : LAST_CACHE_KEY}:${getCacheSuffix(options)}`
	let item = cache.get(cacheKey)

	const sortModifier = first ? -1 : 1

	if (!item) {
		let obj
		if (!searchOptionsHelper.isBest(options))
			obj = await getCollection().findOne(filter, {sort: {date: sortModifier}, projection: {id: 1}})
		else
			obj = (await getCollection().aggregate([
				{$match: filter},
				{$addFields: bestSortFieldAggregator.field},
				{$sort: {[bestSortFieldAggregator.CRITERION_NAME]: sortModifier}},
				{$limit: 1}
			]).toArray())[0]
		item = obj ? obj.id : null

		cache.set(cacheKey, item)
	}

	return item
}