const config = require('../../../../../config')
const DateRange = require('../../../../tools/DateRange')

module.exports = (options) => applyOptions(
	parseQueryLimit,
	parseSort,
	parseBeforeAndAfter,
	parseDateRange
)(options)


const applyOptions = (...functions) => (options) => functions.reduce((total, fn) => fn(total), options)

const parseQueryLimit = (options) => {
	const {first, last} = options
	if (first && last)
		throw 'first and last param can\'t be both provided. Please remove one of them'
	const desiredLimit = first || last || config.defaultQueryLimit
	return {...options, queryLimit: desiredLimit < config.querySanityLimit ? desiredLimit : config.querySanityLimit}
}

const parseSort = (options) => {
	const {sort: passedSort} = options

	if (!passedSort)
		return {...options, sort: 'new'}

	let sort = 'new'
	switch (passedSort.trim().toLowerCase()) {
		case 'random':
			sort = 'random'
			break
		case 'best':
			sort = 'best'
			break
	}
	return {...options, sort}
}

const parseBeforeAndAfter = (options) => {
	if (options.sort === 'best') {
		// the best filter format is equivalent to number so if it's not possible to parse we throw
		if (options.after && isNaN(parseInt(options.after, 10)))
			throw new Error('after param is invalid')
		if (options.before && isNaN(parseInt(options.before, 10)))
			throw new Error('before param is invalid')
		return options
	}


	const myOptions = {...options}
	if (myOptions.before) {
		myOptions.before = new Date(myOptions.before)
		if (isNaN(myOptions.before))
			throw new Error('before needs to be a valid date')
	}
	if (myOptions.after) {
		myOptions.after = new Date(myOptions.after)
		if (isNaN(myOptions.after))
			throw new Error('after needs to be a valid date')
	}

	return myOptions
}


const parseDateRange = (options) => {
	const {dateRange} = options
	if (!dateRange)
		return options

	return {...options, dateRange: DateRange.parse(options.dateRange)}
}