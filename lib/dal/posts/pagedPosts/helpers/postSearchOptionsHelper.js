const config = require('../../../../../config')

const reverseOrder = ({before, after, last, first, sort}) =>
	sort !== 'best'
	&& ((!!last && !first) || (!last && !first && !!before && !after))

module.exports.reverseOrder = reverseOrder
module.exports.isRandom = (options) => options.sort === 'random'
module.exports.isBest = (options) => options.sort === 'best'

module.exports.getQueryLimit = ({first, last}) => {
	if (first && last)
		throw 'first and last param can\'t be both provided. Please remove one of them'
	const desiredLimit = first || last || config.defaultQueryLimit
	return desiredLimit < config.querySanityLimit ? desiredLimit : config.querySanityLimit
}