const parseOptions = require('./helpers/optionsParseAndValidate')
const {getCollection, getLikes} = require('../shared')
const applyFilters = require('./helpers/applyFilters')
const bestSortFieldAggregator = require('./helpers/bestSortFieldAggregation')
const searchOptionsHelper = require('./helpers/postSearchOptionsHelper')
const getFirstAndLast = require('./helpers/getFirstAndLast')

const getQuery = (options, filter, additionalFieldFilter, limit, likeFilterUser) => {
	let aggregationPipeline = []

	if (likeFilterUser)
		aggregationPipeline = aggregationPipeline.concat([
			{$match: {user: likeFilterUser}},
			{
				$lookup: {
					from: 'posts',
					localField: 'postId',
					foreignField: 'id',
					as: 'post'
				}
			},
			{$unwind: '$post'},
			{$replaceRoot: {newRoot: '$post'}}])

	aggregationPipeline.push({$match: filter})

	if (searchOptionsHelper.isRandom(options))
		aggregationPipeline.push({$sample: {size: limit}})
	else if (searchOptionsHelper.isBest(options))
		aggregationPipeline = aggregationPipeline.concat([
			{$addFields: bestSortFieldAggregator.field},
			{$match: additionalFieldFilter},
			{$sort: {[bestSortFieldAggregator.CRITERION_NAME]: -1}},
			{$limit: limit}
		])
	else
		aggregationPipeline = aggregationPipeline.concat([
			{$sort: searchOptionsHelper.reverseOrder(options) ? {date: 1} : {date: -1}},
			{$limit: limit},
			{$sort: {date: -1}}
		])

	aggregationPipeline.concat({$match: {removed: {$exists: false}}})

	return {
		aggregationPipeline,
		collection: likeFilterUser ? getLikes : getCollection
	}
}

const findPosts = async (inputOptions, authenticatedUser) => {
	const options = parseOptions(inputOptions)

	const {mainFilter, additionalFieldFilter} = applyFilters(options)

	const {aggregationPipeline, collection} = getQuery(
		options,
		mainFilter,
		additionalFieldFilter,
		searchOptionsHelper.getQueryLimit(options),
		inputOptions.likedByMe && authenticatedUser ? authenticatedUser : null)

	const query = collection().aggregate(aggregationPipeline)

	const posts = await query
		.toArray()

	return {posts, ...(await getFirstAndLast(inputOptions, mainFilter, authenticatedUser))}
}

module.exports = {findPosts}