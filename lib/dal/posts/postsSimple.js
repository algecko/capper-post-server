const {getCollection} = require('./shared')

module.exports.findIds = (idArray) => getCollection().find({id: {$in: idArray}}).toArray()

module.exports.getPost = (id) =>
	getCollection()
		.findOne({id})