const {getCollection} = require('../shared')

const countWords = (text) =>
	text
		.replace(/[\s"!?.:+*#`˚';\-\/\\\r\n]+/g, ' ')
		.trim()
		.split(' ')
		.length


module.exports = async function () {

	return getCollection()
		.find({size: {$exists: false}})
		.forEach(item => {
			return getCollection().update(item, {$set: {size: countWords(item.text)}})
		})

}