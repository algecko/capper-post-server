const ensureSize = require('./ensureSize')
const oldImages = require('./oldImages')


module.exports = {
	ensureSize,
	oldImages
}