const {getCollection} = require('../shared')

module.exports = async () => {
	getCollection()
		.updateMany({imgDead: {$exists: false}, date: {$lte: new Date('2018-01-01')}}, {$set: {imgDead: true}})
}