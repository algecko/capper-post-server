const db = require('../../db')

module.exports.getCollection = () => db.get().collection('posts')
module.exports.getLikes = () => db.get().collection('likes')