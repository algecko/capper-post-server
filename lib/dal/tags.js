const db = require('../db')

const getCollection = () => db.get().collection('posts')

module.exports.getAllTags = () =>
	getCollection()
		.aggregate([{$unwind: '$tags'}, {
			$group: {
				_id: '$tags',
				count: {$sum: 1}
			}
		}, {$sort: {_id: 1}}])
		.toArray()
		.then(tags => tags.map(tag => ({name: tag._id, postCount: tag.count})))
