const MongoClient = require('mongodb').MongoClient

const state = {
	client: null
}

exports.connect = async (url) => state.client
	? undefined :
	MongoClient.connect(url)
		.then(client => state.client = client)


exports.get = () => state.client.db()

exports.close = async () => !state.client ?
	undefined :
	state.client.close()
		.then(res => {
			state.client = null
			state.mode = null
			return res
		})
