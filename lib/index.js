const server = require('./server')
const db = require('./db')
const config = require('../config')
const startup = require('./startup')

const port = process.env.PORT || config.port
const dbUrl = process.env.DB_URL || config.dbUrl

module.exports = db.connect(dbUrl)
	.then(startup)
	.then(() => {
		server.listen(port, () => {
			console.log(`Started at port ${port}`)
		})
	})
	.catch(err => {
		console.error(err)
		process.exit(1)
	})