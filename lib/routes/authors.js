const express = require('express')
// noinspection JSCheckFunctionSignatures
const router = express.Router()

const authorsDal = require('../dal/authors')

router.get('/', (req, res, next) => {
	authorsDal.getAllAuthors()
		.then(tags => {
			res.json(tags)
		})
		.catch(next)
})

module.exports = router