const express = require('express')
// noinspection JSCheckFunctionSignatures
const router = express.Router()
const metaDal = require('../dal/meta')

router.get('/postsPerDate', (req, res, next) => {
	metaDal.getPostsPerDate()
		.then(ppds => res.json(ppds))
		.catch(next)
})


module.exports = router