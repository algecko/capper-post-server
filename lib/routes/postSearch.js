const express = require('express')
// noinspection JSCheckFunctionSignatures
const router = express.Router()
const postDal = require('../dal/posts')

router.post('/byId', (req, res, next) => {
	postDal.findIds(req.body.ids)
		.then(posts => res.json(posts))
		.catch(next)
})

router.post('/', (req, res, next) => {
	postDal.findPosts(req.body, req.user)
		.then(posts => res.json(posts))
		.catch(next)
})


module.exports = router