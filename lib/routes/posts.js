const express = require('express')
// noinspection JSCheckFunctionSignatures
const router = express.Router()
const postDal = require('../dal/posts')

router.get('/:id', (req, res, next) => {
	postDal.getPost(req.params.id)
		.then(post => {
			if (post) return res.json(post)
			throw {statusCode: 404}
		})
		.catch(next)
})

router.put('/:id/like', (req, res, next) => {
	postDal.like(req.params.id)
		.then(post => {
			if (post) return res.json(post)
			throw {statusCode: 404}
		})
		.catch(next)
})

router.put('/:id/unlike', (req, res, next) => {
	postDal.unlike(req.params.id)
		.then(post => {
			if (post) return res.json(post)
			throw {statusCode: 404}
		})
		.catch(next)
})


module.exports = router