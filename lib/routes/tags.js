const express = require('express')
// noinspection JSCheckFunctionSignatures
const router = express.Router()

const tagDal = require('../dal/tags')

router.get('/', (req, res, next) => {
	tagDal.getAllTags()
		.then(tags => {
			res.json(tags)
		})
		.catch(next)
})

module.exports = router