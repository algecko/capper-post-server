const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const passport = require('passport')
const JwtStrategy = require('passport-jwt').Strategy
ExtractJwt = require('passport-jwt').ExtractJwt

const posts = require('./routes/posts')
const meta = require('./routes/meta')
const tags = require('./routes/tags')
const authors = require('./routes/authors')
const postSearch = require('./routes/postSearch')
const errorHandling = require('./routes/errorHandling')
const jwtTools = require('./tools/jwtTools')

const app = express()

passport.use(new JwtStrategy({
	secretOrKeyProvider: jwtTools.getSecret,
	jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
}, (jwt_payload, done) => {
	done(null, jwt_payload.sub)
}))

app.use(passport.initialize())
app.use(cors({
	origin: process.env.NODE_ENV === 'production' ? /(https?:\/\/opentgc\.com|https?:\/\/95\.216\.148\.182)/ : '*'
}))

app.use(bodyParser.json())

app.use('/posts', posts)
app.use('/tags', tags)
app.use('/authors', authors)
app.use('/meta', meta)
app.use('/postSearch', (req, res, next) => {
	passport.authenticate('jwt', (err, user) => {
		if (user)
			req.user = user
		next()
	})(req, res, next)
}, postSearch)

app.use((req, res) => {
	res.status(404).send('Page not Found')
})

errorHandling(app)

module.exports = app