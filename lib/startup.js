const postScripts = require('./dal/posts').scripts

async function startup() {
	await postScripts.ensureSize()
	await postScripts.oldImages()
}

module.exports = startup