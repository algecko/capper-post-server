class DateRange {
	constructor(rangeStart, rangeEnd) {
		this.rangeStart = new Date(rangeStart)
		this.rangeEnd = new Date(rangeEnd)
	}

	static parse(dateRangeString) {
		if (!dateRangeString)
			return

		const parts = dateRangeString.split('-').map(item => item.trim())

		switch (parts.length) {
			case 1:
				return new ImpreciseDate(parts[0]).getRange()
			case 2:
				return new DateRange(parts[0], parts[1])
			default:
				throw new Error('Unspported date range string: ' + dateRangeString)
		}
	}
}

const datePrecision = {
	day: 'day',
	month: 'month',
	year: 'year'
}

class ImpreciseDate {
	constructor(dateString) {
		let parts = dateString.split('/').map(it => it.trim())
		this.precision = parts.length === 3 ? datePrecision.day :
			parts.length === 2 ? datePrecision.month :
				parts.length === 1 ? datePrecision.year :
					undefined

		if (!this.precision)
			throw new Error(`Date format for ${dateString} not supported. Please use YYYY/mm/dd or similar`)

		parts = parts.concat([undefined, undefined])

		this.parsedDate = new Date(Date.UTC(parts[0], (parts[1] || 1) - 1, parts[2] || 1, 0, 0, 0))
	}

	getRange() {
		let endDate = new Date(this.parsedDate)
		if (this.precision === datePrecision.year) {
			endDate.setFullYear(endDate.getFullYear() + 1)
		}
		if (this.precision === datePrecision.month) {
			endDate.setMonth(endDate.getMonth() + 1)
		}
		if (this.precision === datePrecision.day) {
			endDate.setDate(endDate.getDate() + 1)
		}

		return new DateRange(this.parsedDate, endDate)
	}
}


module.exports = DateRange