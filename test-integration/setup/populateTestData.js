const db = require('../../lib/db')
const testPosts = require('../testData/posts.json')

const seedData = async () => {
	await timeout(300)
	await db.connect('mongodb://localhost:28000/capper')
	await db.get().collection('posts').insert(testPosts.map(tp => ({...tp, date: new Date(tp.date)})))
	await db.close()
}

const timeout = (ms) => {
	console.log(`waiting for ${Math.floor(ms)}ms to ensure container is up`)

	return new Promise(resolve => {
		setTimeout(() => {
			resolve()
		}, 3000)
	})
}

seedData()
	.then(() => {
		console.log('success')
		process.exit()
	})
	.catch((err) => {
		console.error(err)
		process.exit(-1)
	})
