#!/bin/sh

CONTAINER_NAME="capper-int-test"
BASEDIR=$(dirname "$0")

docker stop $CONTAINER_NAME

docker run -d --rm -p 28000:27017 --name $CONTAINER_NAME mongo:latest

node $BASEDIR/populateTestData.js

mocha $BASEDIR/../tests --recursive

docker stop $CONTAINER_NAME