const expect = require('chai').expect

const testPosts = require('../../testData/posts')
const authorsDal = require('../../../lib/dal/authors')

const allAuthors = testPosts.map(tp => tp.author).filter((value, index, self) => self.indexOf(value) === index)

describe('authors-dal', () => {
	describe('getAllAuthors', () => {
		it('returns the expected number of authors for given test data', async () => {
			const authorsResult = await authorsDal.getAllAuthors()
			expect(authorsResult).to.have.length(allAuthors.length)
		})
		it('returns list without duplicates', async () => {
			const authorsResult = await authorsDal.getAllAuthors()
			expect(authorsResult.filter((value, index, self) => self.indexOf(value) === index))
				.to.have.length(authorsResult.length)
		})
		it('returns a list that contains all expected items', async () => {
			const authorsResult = await authorsDal.getAllAuthors()
			expect(allAuthors.find(author => !authorsResult.find(ar => ar.name === author))).to.be.undefined
		})
	})
})