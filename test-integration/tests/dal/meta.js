require('chai').should()

const testPosts = require('../../testData/posts')
const metaDal = require('../../../lib/dal/meta')

const postDateCompare = (date) => testPost => {
	const tpDate = new Date(testPost.date)
	return tpDate.getUTCFullYear() === date.getUTCFullYear()
		&& tpDate.getUTCMonth() === date.getUTCMonth()
		&& tpDate.getUTCDate() === date.getUTCDate()
}

describe('meta-dal', () => {
	describe('getPostsPerDate', () => {
		it('returns elements that contain a filed day that can be parsed as date', async () => {
			const ppd = await metaDal.getPostsPerDate()
			const anItem = ppd[0]
			Date.parse(anItem.day).should.not.be.NaN
		})
		it('returns elements that contain a numeric filed postCount', async () => {
			const ppd = await metaDal.getPostsPerDate()
			const anItem = ppd[0]
			anItem.postCount.should.be.a('number')
		})
		it('sums up posts per day', async () => {
			const ppds = await metaDal.getPostsPerDate()
			ppds.forEach(ppd =>
				ppd.postCount.should.equal(testPosts.filter(postDateCompare(new Date(ppd.day))).length)
			)
		})

	})
})