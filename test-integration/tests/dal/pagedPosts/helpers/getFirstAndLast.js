const chai = require('chai')
chai.use(require('chai-datetime'))
chai.use(require('chai-as-promised'))
chai.should()
const escapeStringRegexp = require('escape-string-regexp')

const testPosts = require('../../../../testData/posts')
const getFirstAndLast = require('../../../../../lib/dal/posts/pagedPosts/helpers/getFirstAndLast')
const sorters = require('../testTools/sorters')

describe('getFirstAndLast', () => {
	const emptyFilter = {$and: []}
	describe('sort=new', () => {
		it('returns the absolute oldest post if no filter is passed', () => {
			const expected = testPosts.sort(sorters.oldestPostSorter)[0]
			return getFirstAndLast({}, emptyFilter).should.eventually.have.property('last').that.equals(expected.id)
		})
		it('returns the absolute newest post if no filter is passed', () => {
			const expected = testPosts.sort(sorters.newestPostSorter)[0]
			return getFirstAndLast({}, emptyFilter).should.eventually.have.property('first').that.equals(expected.id)
		})
		it('applies the filter and only searches for first within those results', () => {
			const refPost = testPosts[0]
			const filter = {$and: [{author: new RegExp(`^${escapeStringRegexp(refPost.author)}$`)}]}
			const posts = testPosts.filter(post => post.author === refPost.author).sort(sorters.newestPostSorter)
			const expected = posts[0]

			return getFirstAndLast({authors: refPost.author}, filter).should.eventually.have.property('first').that.equals(expected.id)
		})
		it('applies the filter and only searches for falst within those results', () => {
			const refPost = testPosts[0]
			const filter = {$and: [{author: new RegExp(`^${escapeStringRegexp(refPost.author)}$`, 'i')}]}
			const posts = testPosts.filter(post => post.author === refPost.author).sort(sorters.oldestPostSorter)

			const expected = posts[0]

			return getFirstAndLast({authors: refPost.author}, filter).should.eventually.have.property('last').that.equals(expected.id)
		})
		it('ignores before param', () => {
			const posts = testPosts.sort(sorters.oldestPostSorter)
			const expected = posts[0]

			return getFirstAndLast({before: '2015'}, emptyFilter).should.eventually.have.property('last').that.equals(expected.id)
		})
		it('ignores after param', () => {
			const posts = testPosts.sort(sorters.newestPostSorter)
			const expected = posts[0]

			return getFirstAndLast({after: '2020'}, emptyFilter).should.eventually.have.property('first').that.equals(expected.id)
		})
	})
	describe('sort=best', () => {
		it('returns the absolute best post as first if no filter is passed', () => {
			const expected = testPosts.sort(sorters.bestSort)[0]
			return getFirstAndLast({sort: 'best'}, emptyFilter).should.eventually.have.property('first').that.equals(expected.id)
		})
		it('returns the absolute worst / oldest post as last if no filter is passed', () => {
			const sorted = testPosts.sort(sorters.bestSort)
			const expected = sorted[sorted.length - 1]
			return getFirstAndLast({sort: 'best'}, emptyFilter).should.eventually.have.property('last').that.equals(expected.id)
		})
		it('applies the filter and only searches for first within those results', () => {
			const refPost = testPosts[4]
			const filter = {$and: [{author: new RegExp(`^${escapeStringRegexp(refPost.author)}$`)}]}
			const posts = testPosts.filter(post => post.author === refPost.author).sort(sorters.bestSort)
			const expected = posts[0]

			return getFirstAndLast({
				authors: refPost.author,
				sort: 'best'
			}, filter).should.eventually.have.property('first').that.equals(expected.id)
		})
		it('applies the filter and only searches for last within those results', () => {
			const refPost = testPosts[4]
			const filter = {$and: [{author: new RegExp(`^${escapeStringRegexp(refPost.author)}$`)}]}
			const posts = testPosts.filter(post => post.author === refPost.author).sort(sorters.bestSort)
			const expected = posts[posts.length - 1]

			return getFirstAndLast({
				authors: refPost.author,
				sort: 'best'
			}, filter).should.eventually.have.property('last').that.equals(expected.id)
		})
		it('ignores before param', () => {
			const sorted = testPosts.sort(sorters.bestSort)
			const expected = sorted[0]
			return getFirstAndLast({
				sort: 'best',
				before: '20120101'
			}, emptyFilter).should.eventually.have.property('first').that.equals(expected.id)
		})
		it('ignores after param', () => {
			const sorted = testPosts.sort(sorters.bestSort)
			const expected = sorted[sorted.length-1]
			return getFirstAndLast({
				sort: 'best',
				after: '50020160101'
			}, emptyFilter).should.eventually.have.property('last').that.equals(expected.id)
		})
	})
})