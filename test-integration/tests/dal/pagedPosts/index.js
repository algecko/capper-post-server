const chai = require('chai')
chai.use(require('chai-datetime'))
chai.use(require('chai-as-promised'))
chai.should()

const testPosts = require('../../../testData/posts')
const pagedPosts = require('../../../../lib/dal/posts/pagedPosts')
const config = require('../../../../config')
const {CRITERION_NAME: bestSortCriterion} = require('../../../../lib/dal/posts/pagedPosts/helpers/bestSortFieldAggregation')

const sorters = require('./testTools/sorters')
const allPostsIntersect = require('./testTools/allPostsIntersect')

describe('pagedPosts', () => {
	describe('findPosts', () => {
		it('returns the newest [default query limit] posts if empty options are passed', () => {
			const expectedLength = testPosts.length < config.defaultQueryLimit ? testPosts.length : config.defaultQueryLimit
			return pagedPosts.findPosts({}).should.eventually.have.property('posts').with.length(expectedLength)
		})
		it('applies authors filter for one author', async () => {
			const author = testPosts[0].author
			const expectedPosts = testPosts.filter(tp => tp.author === author)
			const result = await  pagedPosts.findPosts({
				authors: author
			})
			allPostsIntersect(expectedPosts, result.posts).should.be.true
		})
		it('applies authors filter for multiple authors', async () => {
			const authors = ['Author 1', 'Author 2']
			const expectedPosts = testPosts.filter(tp => authors.includes(tp.author))
			const result = await  pagedPosts.findPosts({
				authors: authors.join(',')
			})
			allPostsIntersect(expectedPosts, result.posts).should.be.true

		})

		it('applies tags filter for one tag', async () => {
			const tag = testPosts[0].tags[0]
			const expectedPosts = testPosts.filter(tp => !!tp.tags.find(compTag => compTag === tag))
			const result = await  pagedPosts.findPosts({
				tags: tag
			})
			allPostsIntersect(expectedPosts, result.posts).should.be.true
		})
		it('applies date range filter for only year', async () => {
			const year = 2018
			const expectedPosts = testPosts.filter(tp => new Date(tp.date).getFullYear() === year)
			const result = await  pagedPosts.findPosts({
				dateRange: `${year}`
			})
			allPostsIntersect(expectedPosts, result.posts).should.be.true
		})
		it('accepts first and returns the first n posts', async () => {
			const first = 3
			const expectedPosts = testPosts.sort(sorters.newestPostSorter).slice(0, first)

			const result = await  pagedPosts.findPosts({
				first
			})
			allPostsIntersect(expectedPosts, result.posts).should.be.true
		})
		it('accepts last and returns the last n posts', async () => {
			const last = 3
			const expectedPosts = testPosts.sort(sorters.oldestPostSorter).slice(0, last)
			const result = await  pagedPosts.findPosts({
				last
			})
			allPostsIntersect(expectedPosts, result.posts).should.be.true
		})

		describe('sort=best', () => {
			it('returns the highest rated posts first', async () => {
				const first = 3
				const expectedPosts = testPosts.sort(sorters.bestSort).slice(0, first)
				const result = await pagedPosts.findPosts({
					sort: 'best',
					first
				})
				allPostsIntersect(expectedPosts, result.posts).should.be.true
			})
			it('accepts the sort criterion as an after param', async () => {
				const first = 3
				const expectedPosts = testPosts.sort(sorters.bestSort).slice(first, first * 2)
				const firstResult = await pagedPosts.findPosts({
					sort: 'best',
					first
				})
				const secondResult = await pagedPosts.findPosts({
					sort: 'best',
					first,
					after: firstResult.posts[firstResult.posts.length - 1][bestSortCriterion]
				})
				allPostsIntersect(expectedPosts, secondResult.posts).should.be.true
			})
		})
	})
})