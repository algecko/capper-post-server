module.exports = (a, b) => a && b
	&& a.length === b.length
	&& !a.find(aItem => !b.find(bItem => bItem.id === aItem.id))