const newestPostSorter = module.exports.newestPostSorter = (a, b) => new Date(b.date).getTime() - new Date(a.date).getTime()

module.exports.oldestPostSorter = (a, b) => new Date(a.date).getTime() - new Date(b.date).getTime()

module.exports.bestSort = (a, b) => {
	const diff = (b.likes || 0) - (a.likes || 0)
	if (diff !== 0)
		return diff
	return newestPostSorter(a, b)
}