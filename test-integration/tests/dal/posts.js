const chai = require('chai')
chai.use(require('chai-as-promised'))
chai.should()

const testPosts = require('../../testData/posts')
const postDal = require('../../../lib/dal/posts')

//const allAuthors = testPosts.map(tp => tp.author).filter((value, index, self) => self.indexOf(value) === index)

describe('posts-dal', () => {

	describe('getPost', () => {
		it('returns the post for the id provided', async () => {
			const thePost = testPosts[0]
			const post = await postDal.getPost(thePost.id)

			post.id.should.equal(thePost.id)
		})
		it('returns null if no post is found', async () => {
			const post = await postDal.getPost('-1')
			chai.expect(post).to.be.null
		})
	})

	describe('likePost', () => {
		it('returns the liked post', async () => {
			const thePost = testPosts.find(tp => !!tp.likes)
			const origPost = await postDal.getPost(thePost.id)
			const post = await postDal.like(origPost.id)
			post.id.should.equal(origPost.id)
		})
		it('increases likes by 1', async () => {
			const thePost = testPosts.find(tp => !!tp.likes)
			const origPost = await postDal.getPost(thePost.id)
			const post = await postDal.like(origPost.id)
			post.likes.should.equal(origPost.likes + 1)
		})
		it('also works if likes is undefined', async () => {
			const thePost = testPosts.find(tp => !tp.likes)
			const origPost = await postDal.getPost(thePost.id)
			const post = await postDal.like(origPost.id)
			post.likes.should.equal(1)
		})
	})

	describe('unlikePost', () => {
		it('returns the unliked post', async () => {
			const thePost = testPosts.find(tp => !!tp.likes)
			const origPost = await postDal.getPost(thePost.id)
			const post = await postDal.unlike(origPost.id)
			post.id.should.equal(origPost.id)
		})
		it('decreses likes by 1', async () => {
			const thePost = testPosts.find(tp => !!tp.likes)
			const origPost = await postDal.getPost(thePost.id)
			const post = await postDal.unlike(origPost.id)
			post.likes.should.equal(origPost.likes - 1)
		})
	})

	describe('findIds', () => {
		it('returns all the posts passed in the ID array', async () => {
			const posts = testPosts.slice(3)

			const result = await postDal.findIds(posts.map(post => post.id))
			result.filter(resPost => !posts.find(p => p.id === resPost.id)).should.have.length(0)
		})
		it('returns an empty array if no ids are passed', async () => {
			(await postDal.findIds([])).should.be.empty
		})
		it('throws if the parameter missing', () => {
			return postDal.findIds().should.eventually.be.rejected
		})
	})
})