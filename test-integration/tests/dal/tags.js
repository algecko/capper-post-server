const expect = require('chai').expect

const testPosts = require('../../testData/posts')
const tags = require('../../../lib/dal/tags')

const testTags = testPosts.reduce((total, current) => total.concat(current.tags), [])
	.filter((value, index, self) => self.indexOf(value) === index)

describe('tags-dal', () => {
	describe('getAllTags', () => {
		it('returns the expected number of tags for given test data', async () => {
			const tagResult = await tags.getAllTags()
			expect(tagResult).to.have.length(testTags.length)
		})
		it('returns list without duplicates', async () => {
			const tagResult = await tags.getAllTags()
			expect(tagResult.filter((value, index, self) => self.indexOf(value) === index))
				.to.have.length(tagResult.length)
		})
		it('returns a list that contains all expected items', async () => {
			const tagResult = await tags.getAllTags()
			expect(testTags.find(tt => !tagResult.find(tr => tr.name === tt))).to.be.undefined
		})
		it('sums up all the posts correctly', async () => {
			const tagResult = await tags.getAllTags()
			expect(tagResult.find(tr => tr.postCount !== testPosts.filter(tp => tp.tags.includes(tr.name)).length)).to.be.undefined
		})
	})
})