const chai = require('chai')
chai.use(require('chai-datetime'))
chai.should()

const applyFilters = require('../../../../../lib/dal/posts/pagedPosts/helpers/applyFilters')


describe('applyFilters', () => {
	it('returns an empty mainFilter for empty options', () => {
		applyFilters({}).should.have.property('mainFilter').that.is.empty
	})
	it('returns an empty additionalFieldFilter for empty options', () => {
		applyFilters({}).should.have.property('additionalFieldFilter').that.is.empty
	})
	//more tests should be pasted here
})