const chai = require('chai')
chai.use(require('chai-as-promised'))
chai.should()

const getFirstAndLast = require('../../../../../lib/dal/posts/pagedPosts/helpers/getFirstAndLast')


describe('getFirstAndLast', () => {
	it('returns null for first if called for random', () => {
		return getFirstAndLast({sort: 'random'}, {}).should.eventually.have.property('first').that.is.null
	})
	it('returns null for last if called for random', () => {
		return getFirstAndLast({sort: 'random'}, {}).should.eventually.have.property('last').that.is.null
	})
})