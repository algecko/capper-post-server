const chai = require('chai')
chai.use(require('chai-datetime'))
chai.should()

const validator = require('../../../../../lib/dal/posts/pagedPosts/helpers/optionsParseAndValidate')
const config = require('../../../../../config')


describe('optionsParseAndValidate', () => {
	it('returns an object', () => {
		validator({}).should.be.an('object')
	})

	describe('first, last', () => {
		it('returns the default limit if no params are passed', () => {
			validator({}).should.have.property('queryLimit').that.equals(config.defaultQueryLimit)
		})
		it('returns the the max query limit if a higher limit than expected is passed', () => {
			validator({first: (config.querySanityLimit + 1)}).should.have.property('queryLimit').that.equals(config.querySanityLimit)
		})
	})

	describe('sort', () => {
		it('returns new if no other sort criterion is passed', () => {
			validator({}).should.have.property('sort').that.equals('new')
		})
		it('returns new if an incorrect value has been passed', () => {
			validator({sort: 'dummy'}).should.have.property('sort').that.equals('new')
		})
		it('ignores casing for passed values', () => {
			validator({sort: 'RaNdOm'}).should.have.property('sort').that.equals('random')
		})
	})

	describe('limiters', () => {
		it('throws if sort = best and before is not a number string', () => {
			validator.bind(null, ({sort: 'best', before: 'asdf'})).should.throw()
		})
		it('throws if sort = best and after is not a number string', () => {
			validator.bind(null, ({sort: 'best', after: 'asdf'})).should.throw()
		})
		it('returns the same string if before is passed in sort best', () => {
			const string = '123420170521'
			validator({sort: 'best', before: string}).should.have.property('before').that.equals(string)
		})
		it('returns the same string if after is passed in sort best', () => {
			const string = '123420170521'
			validator({sort: 'best', after: string}).should.have.property('after').that.equals(string)
		})
		it('parses the passed value for after as ISO date for non best sort', () => {
			const theDate = new Date(Date.UTC(2018, 1, 2, 3, 4, 5))
			validator({after: theDate.toISOString()}).should.have.property('after').that.equalDate(theDate)
		})
		it('parses the passed value for before as ISO date for non best sort', () => {
			const theDate = new Date(Date.UTC(2018, 1, 2, 3, 4, 5))
			validator({before: theDate.toISOString()}).should.have.property('before').that.equalDate(theDate)
		})
		it('throws if passed value is not a valid date', () => {
			validator.bind(null, ({before: 'dummy'})).should.throw()
		})
	})

	describe('dateRange', () => {
		it('is parsed as a DateRange', () => {
			validator({dateRange:'2018'}).should.have.property('dateRange').that.hasOwnProperty('rangeStart')
		})
	})
})