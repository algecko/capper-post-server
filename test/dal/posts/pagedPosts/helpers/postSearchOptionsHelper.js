const chai = require('chai')
chai.should()

const theHelper = require('../../../../../lib/dal/posts/pagedPosts/helpers/postSearchOptionsHelper')
const config = require('../../../../../config')

describe('postSearchOptionsHelper', () => {
	describe('reverseOrder', () => {
		it('is false if sort parameter is \'best\'', () => {
			theHelper.reverseOrder({sort: 'best'}).should.be.false
		})
		it('is true if last is passed but first isn\'t', () => {
			theHelper.reverseOrder({sort: 'notBest', last: 50}).should.be.true
		})
		it('is true only before is passed', () => {
			theHelper.reverseOrder({sort: 'notBest', before: 'asdf'}).should.be.true
		})
		it('is false if after is passed', () => {
			theHelper.reverseOrder({sort: 'notBest', after: 'asdf'}).should.be.false
		})
	})

	describe('getQueryLimit', () => {
		it('returns the default limit if no params are passed', () => {
			theHelper.getQueryLimit({}).should.equal(config.defaultQueryLimit)
		})
		it('returns the the max query limit if a higher limit than expected is passed', () => {
			theHelper.getQueryLimit({first: (config.querySanityLimit + 1)}).should.equal(config.querySanityLimit)
		})
	})
})