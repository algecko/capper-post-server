const request = require('supertest')
const server = require('../../lib/server')
const sinon = require('sinon')

const authorDal = require('../../lib/dal/authors')

describe('/authors', () => {
	describe('get /', () => {
		it('returns whatever result is returned by dal', (done) => {
			const result = {dummy: 1}
			sinon.stub(authorDal, 'getAllAuthors').resolves(result)
			request(server)
				.get('/authors')
				.expect(200, result, done)
		})
	})
})