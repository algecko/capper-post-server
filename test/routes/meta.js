const request = require('supertest')
const server = require('../../lib/server')
const sinon = require('sinon')

const metaDal = require('../../lib/dal/meta')

describe('/meta', () => {
	let sandbox
	beforeEach(function () {
		sandbox = sinon.createSandbox()
	})

	afterEach(function () {
		sandbox.restore()
	})

	describe('get /postsPerDate', () => {
		it('returns whatever is returned by metadal', (done) => {
			const result = {dummy: 1}
			sandbox.stub(metaDal, 'getPostsPerDate').resolves(result)
			request(server)
				.get('/meta/postsPerDate')
				.expect(200, result, done)
		})

	})
})