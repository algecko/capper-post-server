const request = require('supertest')
const server = require('../../lib/server')
const sinon = require('sinon')

const postDal = require('../../lib/dal/posts')

describe('/posts', () => {
	let sandbox
	beforeEach(function () {
		sandbox = sinon.createSandbox()
	})

	afterEach(function () {
		sandbox.restore()
	})

	describe('get /:id', () => {
		it('returns whatever result is returned by postdal', (done) => {
			const result = {dummy: 1}
			sandbox.stub(postDal, 'getPost').resolves(result)
			request(server)
				.get('/posts/dummy')
				.expect(200, result, done)
		})
		it('calls postDal with the ID passed', (done) => {
			const stub = sandbox.stub(postDal, 'getPost').resolves({})
			const id = 'theId'
			request(server)
				.get(`/posts/${id}`)
				.end(() => {
					sinon.assert.calledWith(stub, id)
					done()
				})
		})
		it('returns 404 in case no post is found', (done) => {
			sandbox.stub(postDal, 'getPost').resolves(null)
			request(server)
				.get('/posts/dummy')
				.expect(404, done)
		})
	})

	describe('put /:id/like', () => {
		it('returns whatever result is returned by postDal.like', (done) => {
			const result = {dummy: 1}
			sandbox.stub(postDal, 'like').resolves(result)
			request(server)
				.put('/posts/dummy/like')
				.expect(200, result, done)
		})
		it('calls postDal with the ID passed', (done) => {
			const stub = sandbox.stub(postDal, 'like').resolves({})
			const id = 'theId'
			request(server)
				.put(`/posts/${id}/like`)
				.end(() => {
					sinon.assert.calledWith(stub, id)
					done()
				})
		})
		it('returns 404 in case no post is found', (done) => {
			sandbox.stub(postDal, 'like').resolves(null)
			request(server)
				.put('/posts/dummy/like')
				.expect(404, done)
		})
	})

	describe('put /:id/unlike', () => {
		it('returns whatever result is returned by postDal.unlike', (done) => {
			const result = {dummy: 1}
			sandbox.stub(postDal, 'unlike').resolves(result)
			request(server)
				.put('/posts/dummy/unlike')
				.expect(200, result, done)
		})
		it('calls postDal with the ID passed', (done) => {
			const stub = sandbox.stub(postDal, 'unlike').resolves({})
			const id = 'theId'
			request(server)
				.put(`/posts/${id}/unlike`)
				.end(() => {
					sinon.assert.calledWith(stub, id)
					done()
				})
		})
		it('returns 404 in case no post is found', (done) => {
			sandbox.stub(postDal, 'unlike').resolves(null)
			request(server)
				.put('/posts/dummy/unlike')
				.expect(404, done)
		})
	})
})