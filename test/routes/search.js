const request = require('supertest')
const server = require('../../lib/server')
const sinon = require('sinon')

const postDal = require('../../lib/dal/posts')

describe('/search', () => {
	let sandbox
	beforeEach(function () {
		sandbox = sinon.createSandbox()
	})

	afterEach(function () {
		sandbox.restore()
	})

	describe('post /byId', () => {
		it('returns whatever result is returned by postdal', (done) => {
			const result = {dummy: 1}
			sandbox.stub(postDal, 'findIds').resolves(result)
			request(server)
				.post('/postSearch/byId')
				.expect(200, result, done)
		})
		it('passes on the id posted', (done) => {
			const ids = [1, 2, 3]
			const stub = sandbox.stub(postDal, 'findIds').resolves([])
			request(server)
				.post('/postSearch/byId')
				.send({ids})
				.end(() => {
					sinon.assert.calledWith(stub, ids)
					done()
				})
		})
	})

	describe('post', () => {
		it('returns whatever result is returned by postdal.findPosts', (done) => {
			const result = [{dummy: 1}]
			sandbox.stub(postDal, 'findPosts').resolves(result)
			request(server)
				.post('/postSearch')
				.expect(200, result, done)
		})
		it('passed the body posted as options to findPosts', (done) => {
			const body = {
				sort: 'best'
			}
			const stub = sandbox.stub(postDal, 'findPosts').resolves([])
			request(server)
				.post('/postSearch')
				.send(body)
				.end(() => {
					sinon.assert.calledWith(stub, body)
					done()
				})
		})
	})
})