const request = require('supertest')
const server = require('../../lib/server')
const sinon = require('sinon')

const tagDal = require('../../lib/dal/tags')

describe('/tags', () => {
	describe('get /', () => {
		it('returns whatever result is returned by dal', (done) => {
			const result = {dummy: 1}
			sinon.stub(tagDal, 'getAllTags').resolves(result)

			request(server)
				.get('/tags')
				.expect(200, result, done)
		})
	})
})