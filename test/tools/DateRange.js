const chai = require('chai')
chai.should()

const DateRange = require('../../lib/tools/DateRange')

describe('DateRange', () => {
	describe('parse', () => {
		describe('single param', () => {
			it('parses start date with YYYY to the first of the year UTC', () => {
				const year = 2018
				const dr = DateRange.parse('' + year)
				dr.rangeStart.toUTCString().should.equal(new Date(Date.UTC(year, 0, 1, 0, 0, 0, 0)).toUTCString())
			})
			it('parses end date with YYYY to the first of the next year UTC', () => {
				const year = 2018
				const dr = DateRange.parse('' + year)
				dr.rangeEnd.toUTCString().should.equal(new Date(Date.UTC(year + 1, 0, 1, 0, 0, 0, 0)).toUTCString())
			})

			it('parses start date with YYYY/mm to the first of the month UTC', () => {
				const year = 2018
				const month = 1
				const dr = DateRange.parse(`${year}/${month}`)
				dr.rangeStart.toUTCString().should.equal(new Date(Date.UTC(year, month - 1, 1, 0, 0, 0, 0)).toUTCString())
			})
			it('parses end date with YYYY/mm to the first of the next month UTC', () => {
				const year = 2018
				const month = 1
				const dr = DateRange.parse(`${year}/${month}`)
				dr.rangeEnd.toUTCString().should.equal(new Date(Date.UTC(year, month, 1, 0, 0, 0, 0)).toUTCString())
			})

			it('parses start date with YYYY/mm/dd to to midnight of the day UTC', () => {
				const year = 2018
				const month = 3
				const day = 2
				const dr = DateRange.parse(`${year}/${month}/${day}`)
				dr.rangeStart.toUTCString().should.equal(new Date(Date.UTC(year, month - 1, day, 0, 0, 0, 0)).toUTCString())
			})
			it('parses end date with YYYY/mm/dd to to midnight of the next day UTC', () => {
				const year = 2018
				const month = 3
				const day = 2
				const dr = DateRange.parse(`${year}/${month}/${day}`)
				dr.rangeEnd.toUTCString().should.equal(new Date(Date.UTC(year, month - 1, day + 1, 0, 0, 0, 0)).toUTCString())
			})
		})
	})
})